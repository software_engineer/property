/* $(document).on("click",".submitFeedback",function(e){
	e.preventDefault();

	if (
		$("#userName").val() == "" ||
		$("#userContact").val() == "" ||
		$("#userComment").val() == "" 
	){
		
	} else {		
		$("#userName").val("");
		$("#userContact").val("");
		$("#userComment").val("");
	}
}); */

$(document).on("keyup","input[type='text'].validateAlphabetNSpace",function(){
    this.value = this.value.replace(/[^\w\s]/gi,''); // remove special char
    this.value = this.value.replace(/[^a-zA-Z ]/g,'');// allow only alphabets
});
/* $(document).on("keyup","input[type='email']",function(){ 
    this.value = this.value.replace(/\s/g,'');
});
$(document).on("change","input[type='email']",function(){ 
    this.value = this.value.replace(/\s/g,'');
    var email = this.value;

    var regEx = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,10}$/; // added from the checkout.js
       

	var validEmail = regEx.test(email);
    
}); */

$("form#contactForm").submit(function(e){

    e.preventDefault();
    
	
	

    var requestConsultationFormData = new FormData(this);
	
	blockFormSubmit(true);
	$.ajax({
		url: baseURL+"Welcome/contactForm",
		type: 'POST',
		data: requestConsultationFormData,
		success: function (data) {
		
			//var data = JSON.parse(data);

			if (data.success == "1") {
				resetForm(true);
			} else {
				resetForm(false);
			}
		},
		error:function (jqXHR,textStatus,errorThrown) {
			blockFormSubmit(false);
		},
		cache: false,
		contentType: false,
		processData: false
	});
});

function blockFormSubmit(block){
	if (block) {
		$("#contactForm .submitContact").prop("disabled",true);
		$("#contactForm .submitContact").html(loader);
	} else {
		$("#contactForm .submitContact").html("Submit");
		$("#contactForm .submitContact").prop("disabled",false);
	}
}

function resetForm(success){
	if (success) {
		$("#contactForm .submitContact").addClass("successButton-feedback-temp").html("Thank You !");
		setTimeout(function(){
			$("#contactForm .submitContact").removeClass("successButton-feedback-temp").html("Submit");
			$("#contactForm .requiredInput").val("");
			$("#contactForm textarea").val("");
			blockFormSubmit(false);
		},1500);
	} else {
		
	}
}