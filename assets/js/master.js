const baseURL   =   $("#baseUrl").val();
const loader    =   '<div class="spinner-border text-dark" role="status"><span class="sr-only">Loading...</span></div>';

function slideshow(){
    var x = document.getElementById("check-class");
    if (x.style.display === "none" || x.style.display === "") {
        x.style.display = "block";
        $(".mobile-icon").html("").html('<i class="fa fa-times"></i>');
    }else{
        x.style.display = "none";
        $(".mobile-icon").html("").html('<i class="fa fa-bars"></i>');
    }
}

$(document).on("click",".main-nav > li",function(){
    slideshow();
});