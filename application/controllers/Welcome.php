<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			$this->load->model('Welcome_model');
	}

	public function index()
	{
		$data = [
			"seo"	=>	[],
			"css"	=>	[
				base_url()."assets/css/welcome.css",
			],
			"js"	=>	[
				base_url()."assets/js/json2html-master/json2html.js",
				base_url()."assets/js/welcome.js"
			],
		];


		$this->load->view('/sections/header1.php',$data);
		$this->load->view('welcome_message',$data);
		$this->load->view('/sections/footer1.php',$data);
	}

	public function contactForm(){
		header('Access-Control-Allow-Origin: *');
		header('Content-type: application/json');
		
		echo json_encode($this->Welcome_model->contactForm($_POST));
	}

	/* public function menu()
	{
		$data = [
			"seo"	=>	[],
			"css"	=>	[],
			"js"	=>	[],
		];


		$this->load->view('/sections/header1.php',$data);
		$this->load->view('menu',$data);
		$this->load->view('/sections/footer1.php',$data);
	}

	public function about()
	{
		$data = [
			"seo"	=>	[],
			"css"	=>	[],
			"js"	=>	[],
		];


		$this->load->view('/sections/header1.php',$data);
		$this->load->view('about',$data);
		$this->load->view('/sections/footer1.php',$data);
	} */
}
