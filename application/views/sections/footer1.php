	<script src="<?php echo base_url(); ?>assets/js/bootstrap-4.5.0-dist/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/notify.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/master.js"></script>
	<?php 
		if ($js) {
			foreach ($js as $key => $singleLink) {
				?>
				<script src="<?php echo $singleLink; ?>"></script>
				<?php
			}
		}
	?>
</body>
</html>