<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		if(ENVIRONMENT == "production"){
	?>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-105309754-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-105309754-2');
    </script>
	<?php
		}
	?>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="<?php echo base_url(); ?>/assets/images/logo.png" type="image/gif">

	<title>Noel Pereira Farm Land</title>
	<meta name="description" content="Find the Farm Land in Mumbai within your budget - The Best deal on Agricultural Land for sale in Mumbai,New Farm Land in Mumbai for Sale, Verified seller of Agricultural Land in Mumbai." />
	<meta name="keywords" content="Farm Land for sale in Mumbai,Agricultural Land for sale in Mumbai,Buy Farm Land in Mumbai,Buy Agricultural Land in Mumbai,New Farm Land in Mumbai,Agricultural/Farm Land for sale in Mumbai,Selling Farm Land in Mumbai"/>
	<link rel="dns-prefetch" href="//www.google-analytics.com/">
	<link rel="dns-prefetch" href="//www.googletagmanager.com/">
	<link rel="dns-prefetch" href="//www.facebook.com/">
	<link rel="dns-prefetch" href="//www.google.co.in/">
	<link rel="dns-prefetch" href="//www.google.com/">
	<link rel="preconnect" href="//fonts.googleapis.com">

	<script src="https://use.fontawesome.com/b5a2d03687.js"></script>
	<link href="https://fonts.googleapis.com/css2?family=Varela+Round&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-4.5.0-dist/bootstrap.min.css"></script> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css"></script> 
	<?php 
		if ($seo) {
			foreach ($seo as $key => $singleMeta) {
				?>
				<meta name="<?php echo $singleMeta["name"]; ?>" content="<?php echo $singleMeta["content"]; ?>">
				<?php
			}
		}
		if ($css) {
			foreach ($css as $key => $singleLink) {
				?>
				<link rel="stylesheet" type="text/css" href="<?php echo $singleLink; ?>"></script>
				<?php
			}
		}
	?>
	<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
</head>
<body>
<input type="hidden" id="baseUrl" value="<?php echo base_url(); ?>" />