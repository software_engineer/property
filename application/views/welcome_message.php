<header class="header">
	<nav>
		<div class="div-row clearfix">
			<img src="<?php echo base_url(); ?>/assets/images/logo.png" class="logo" alt="Noel Pereira Farm Land">
			<!-- <ul class="main-nav" id="check-class">
				<li><a href="">About</a></li>
				<li><a href="#feedback">Contact</a></li>
			</ul>
			<div class="mobile-icon" onclick="slideshow()"><i class="fa fa-bars"></i></div> -->
		</div>
	</nav>
	<div class="main-content">
		<h1>Noel Pereira Farm Land</h1>
		<a class="btn btn-nav" href="#theContactForm">Contact</a>
	</div>
	<div class="main-content-below">
		<div>
			<ul class="features">
				<li>
					Palghar ( 1 hr. )
				</li>
				<li>
					Manor ( 30 min. )
				</li>
				<li>
					Vikramgad ( 14 min. )
				</li>
				<li>
					Essential Documents Ready
				</li>
				<li>
					7/12 Ready
				</li>
				<li>
					Surveyed on 20th. October 2020
				</li>
			</ul>
		</div>
	</div>
</header>
<div class="container-fluid welcome-container" >
	<div id="feedback" class="row feedback-section">
		<div id="theContactForm" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<form id="contactForm" method="post" enctype="multipart/form-data" autocomplete="off" class="half-section">
				<label class="section-title">Contact Form</label>
				<div class="form-group">
					<label for="userName">Name</label>
					<input name="name" type="text" class="form-control validateAlphabetNSpace requiredInput" id="userName" placeholder="">
				</div>
				<div class="form-group">
					<label for="userContact">Contact Number</label>
					<input name="number" type="tel" class="form-control requiredInput" id="userContact" onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" placeholder="">
				</div>
				<div class="form-group">
					<label for="userComment">Queries</label>
    				<textarea name="queries" class="form-control " id="userComment" rows="3"></textarea>
				</div>
				<button class="btn-submit-feedback submitContact" type="submit" value="submit">Submit</button>
			</form>
		</div>
	</div>
</div>