<?php

class Welcome_model extends CI_Model {

    public $title;
    public $content;
    public $date;

    public function contactForm()
    {
        $this->load->library('postmark');
        
        $this->postmark->subject('NPFL Buyer');
        $this->postmark->to('sonyxperiaaviator@gmail.com', 'Xperia Aviator');
        $this->postmark->tag('npflQuery');

        $html = "<html><lable>Name: ".$_POST["name"]."</label><br/><lable>Contact: ".$_POST["number"]."</label><br/> Query: <p>".$_POST["queries"]."</p></html>";
        $this->postmark->message_html($html);
        $this->postmark->template_id(null);
        $response = $this->postmark->send();

        $return=[];
        if($response["ErrorCode"] == 0){
            $return["success"] = 1;
            //$return["data"] = $response;
        }else{
            $return["success"] = 0;
            //$return["data"] = $response;
        }

        return $return;
    }

}

?>